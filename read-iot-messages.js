const { EventHubConsumerClient } = require("@azure/event-hubs");

const connectionString = 'Endpoint=sb://ihsuprodblres091dednamespace.servicebus.windows.net/;SharedAccessKeyName=iothubowner;SharedAccessKey=PNtnXmUIqJwQZdcYMbskdB8lox7f1PyYCNAjvjW7YUM=;EntityPath=iothub-ehub-master-iot-3682874-ebff16fd9f';
var printError = function (err) {
  console.log(err.message);
};

var printMessages = function (messages) {
  for (const message of messages) {
    console.log("Telemetry received: ");
    console.log(JSON.stringify(message.body));
    console.log("Properties (set by device): ");
    console.log(JSON.stringify(message.properties));
    console.log("System properties (set by IoT Hub): ");
    console.log(JSON.stringify(message.systemProperties));
    console.log("");
  }
};

async function main() {
  console.log("IoT Hub Quickstarts - Read device to cloud messages.");
  const clientOptions = {};

  const consumerClient = new EventHubConsumerClient("$Default", connectionString, clientOptions);

  consumerClient.subscribe({
    processEvents: printMessages,
    processError: printError,
  });
}

main().catch((error) => {
  console.error("Error running sample:", error);
});
